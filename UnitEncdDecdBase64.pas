unit UnitEncdDecdBase64;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
	System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
	Vcl.Menus, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, EncdDecd;
  // EncodeBase64, DecodeBase64を利用するためにEncdDecdを追加してください。

type
	TFormEncdDecdBase64 = class(TForm)
    ButtonEncode: TButton;
    ButtonDecode: TButton;
    Memo: TMemo;
    procedure ButtonEncodeClick(Sender: TObject);
    procedure ButtonDecodeClick(Sender: TObject);
	private
		{ Private 宣言 }
	public
		{ Public 宣言 }
	end;

var
	FormEncdDecdBase64: TFormEncdDecdBase64;

implementation

{$R *.dfm}

procedure TFormEncdDecdBase64.ButtonEncodeClick(Sender: TObject);
var
Stream     : TMemoryStream;
EncodedText: AnsiString;
begin

	// メモリーストリームを生成する
	Stream := TMemoryStream.Create;

  // 画像ファイルを読み込む
	Stream.LoadFromFile('.\image.jpg');

  // Base64にエンコードする  ココから↓-------------------
  Memo.Lines.Text := EncodeBase64(Stream.Memory, Stream.Size);
  // Base64にエンコードする  ココまで↑-------------------

  // メモリーストリームを解放する
 	Stream.Free;


end;

procedure TFormEncdDecdBase64.ButtonDecodeClick(Sender: TObject);
var
  Stream: TFileStream;
  B: TBytes;
begin


	// ファイルストリームの生成
  Stream := TFileStream.Create('.\image_decoded.jpg', fmCreate);

  // Base64にデコードする  ココから↓-------------------
  B := DecodeBase64(Memo.Lines.Text);
  // Base64にデコードする  ココまで↑-------------------

  // ファイルにでーたを書き込む
  Stream.Write(B[0], Length(B));

  // ファイルストリームを解放する
  Stream.Free;

end;

end.
