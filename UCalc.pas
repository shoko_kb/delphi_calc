unit UCalc;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
	System.Classes, Vcl.Graphics,
	Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls, Vcl.ExtCtrls;

type
	TOperator = (
		/// @enum 演算子なし
		mtNone,
		/// @enum 加算
		mtAdd,
		/// @enum 減算
		mtDiff,
		/// @enum 乗算
		mtMultiply,
		/// @enum 除算
		mtDiv
	);
	TFormCalc = class(TForm)				 /// 電卓フォーム
		ButtonFloatingPoint: TButton;	 /// ボタン[浮動小数点]
		Button0: TButton;							 /// ボタン[0]
		Button1: TButton;							 /// ボタン[1]
		Button2: TButton;							 /// ボタン[2]
		Button3: TButton;							 /// ボタン[3]
		Button4: TButton;							 /// ボタン[4]
		Button5: TButton;							 /// ボタン[5]
		Button6: TButton;							 /// ボタン[6]
		Button7: TButton;							 /// ボタン[7]
		Button8: TButton;							 /// ボタン[8]
		Button9: TButton;							 /// ボタン[9]
		ButtonAdd: TButton;						 /// ボタン[加算]
		ButtonSubtract: TButton;			 /// ボタン[減算]
		ButtonMultiply: TButton;			 /// ボタン[乗算]
		ButtonDivide: TButton;				 /// ボタン[除算]
		ButtonClear: TButton;					 /// ボタン[クリア]
		ButtonBackSpace: TButton;			 /// ボタン[バックスペース]
		ButtonEqual: TButton;					 /// ボタン[イコール]
		Value: TEdit;									 /// エディットボックス[数値表示部分]
		procedure FormCreate(sender: TObject);
		procedure Button0Click(sender: TObject);
		procedure Button1Click(sender: TObject);
		procedure Button2Click(sender: TObject);
		procedure Button3Click(sender: TObject);
		procedure Button4Click(sender: TObject);
		procedure Button5Click(sender: TObject);
		procedure Button6Click(sender: TObject);
		procedure Button7Click(sender: TObject);
		procedure Button8Click(sender: TObject);
		procedure Button9Click(sender: TObject);
		procedure ButtonClearClick(sender: TObject);
		procedure ButtonFloatingPointClick(sender: TObject);
		procedure ButtonAddClick(sender: TObject);
		procedure ButtonSubtractClick(sender: TObject);
		procedure ButtonMultiplyClick(sender: TObject);
		procedure ButtonDivideClick(sender: TObject);
		procedure ButtonBackSpaceClick(sender: TObject);
		procedure ButtonEqualClick(sender: TObject);
		procedure FormKeyDown(sender: TObject; var Key: Word; Shift: TShiftState);
	private
	{ Private 宣言 }
		const
			VALUE_MAX_LENGTH = 12;
		var
			m_operator : Integer;		 /// 四則演算
			m_currentValue : Double; /// 現在の計算値
			m_parameter : Double;		 /// 新しく入力された引数
			function SetParameter(newValue: string): Boolean;
			function AppendNumber(newNumber : string) : Boolean;
			function GetParameter: Double;
			function BackSpace: Boolean;
	end;

var
	FormCalc: TFormCalc;

implementation

{$R *.dfm}

{**
 *  ボタン[9]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.Button9Click(sender: TObject);
begin
	AppendNumber('9');
end;

{**
 *  ボタン[8]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.Button8Click(sender: TObject);
begin
	AppendNumber('8');
end;

{**
 *  ボタン[7]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.Button7Click(sender: TObject);
begin
	AppendNumber('7');
end;

{**
 *  ボタン[6]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.Button6Click(sender: TObject);
begin
	AppendNumber('6');
end;

{**
 *  ボタン[5]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.Button5Click(sender: TObject);
begin
	AppendNumber('5');
end;

{**
 *  ボタン[4]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.Button4Click(sender: TObject);
begin
	AppendNumber('4');
end;


{**
 *  ボタン[3]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.Button3Click(sender: TObject);
begin
	AppendNumber('3');
end;


{**
 *  ボタン[2]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.Button2Click(sender: TObject);
begin
	AppendNumber('2');
end;


{**
 *  ボタン[1]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.Button1Click(sender: TObject);
begin
	AppendNumber('1');
end;

{**
 *  ボタン[0]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.Button0Click(sender: TObject);
begin
	AppendNumber('0');
end;

{**
 *  ボタン[加算]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.ButtonAddClick(sender: TObject);
begin
	// TODO : 未実装_加算
end;

{**
 *  ボタン[減算]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.ButtonSubtractClick(sender: TObject);
begin
	// TODO : 未実装_減算
end;

{**
 *  ボタン[乗算]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.ButtonMultiplyClick(sender: TObject);
begin
	// TODO : 未実装_乗算
end;

{**
 *  ボタン[除算]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.ButtonDivideClick(sender: TObject);
begin
	// TODO : 未実装_除算
end;

{**
 *  ボタン[クリア]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.ButtonClearClick(sender: TObject);
begin
	SetParameter('0');
end;

{**
 *  ボタン[バックスペース]押下時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.ButtonBackSpaceClick(sender: TObject);
begin

		BackSpace;

end;

{**
 *  ボタン[イコール]押下時、<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.ButtonEqualClick(sender: TObject);
begin
	GetParameter;
end;

{**
 *  ボタン[浮動小数点]押下時、<br/>
 *　浮動小数点を入力する
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.ButtonFloatingPointClick(sender: TObject);
begin
	AppendNumber('.');
end;

{**
 *  キーダウン時、<br/>
 *	数字を入力する
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.FormKeyDown(sender: TObject; var key: Word; shift: TShiftState);
begin
	case key of
		ord('0')	:	AppendNumber('0');
		ord('1')	:	AppendNumber('1');
		ord('2')	:	AppendNumber('2');
		ord('3')	:	AppendNumber('3');
		ord('4')	:	AppendNumber('4');
		ord('5')	:	AppendNumber('5');
		ord('6')	:	AppendNumber('6');
		ord('7')	:	AppendNumber('7');
		ord('8')	:	AppendNumber('8');
		ord('9')	:	AppendNumber('9');
		VK_BACK	 :	BackSpace;
		VK_DELETE : SetParameter('0');
	end;
end;

{**
 *  フォーム作成時､<br/>
 *　
 *  @param	sender	送信元オブジェクト
 *}
procedure TFormCalc.FormCreate(sender: TObject);
var
	currentValue : string;
begin
	m_currentValue := 0;
  currentValue := FloatToStr(m_currentValue);
	SetParameter(currentValue);
end;

{**
 * 数字を設定する
 * @param	newValue	追加する数字
 * @return True 	成功
 * @return False	失敗
 *}
function TFormCalc.SetParameter(newValue: string): Boolean;
begin

	// TODO : 最大文字列長を超過した場合に、切り捨てを行う
	Value.Text := newValue;

	SetParameter := true;

end;


{**
 * 数字を追加する
 * @param	newNumber	追加する数字
 * @return True 	成功
 * @return False	失敗
 *}
function TFormCalc.AppendNumber(newNumber : string) : Boolean;
var
	nResultLen: Integer;						// 結果表示ラベルの文字列長
	strParam: string;							 // 結果表示ラベルの文字列
	nSearchFloatingPoint : Integer; // 浮動小数点を含むかを判断するフラグ(含む場合:0以外、含まない場合:0)
begin

	nResultLen := Value.GetTextLen;
	strParam := Value.Text;

	if (nResultLen < VALUE_MAX_LENGTH) then begin

		if (newNumber = '.') then begin

			// 追加する数字が浮動小数点の場合、
			// 浮動小数点を2つ以上追加しないようにする
			nSearchFloatingPoint := Pos('.', strParam);
			if (nSearchFloatingPoint = 0) then begin
				strParam := strParam + newNumber
			end;

		end else begin

			// 数字を入力する場合には
			// 直前の入力値が0の場合入力数字を上書き、
			// そのほかの場合は数字を末尾に追加する
			if (strParam = '0') then
				strParam := newNumber
			else
				strParam := strParam + newNumber;

		end;

		Value.Text := strParam;

	end;

	AppendNumber := true;
end;

{**
 * 変数を取得する
 * @return 変数
 *}
function TFormCalc.GetParameter: Double;
var
	strParam : string;
begin
	strParam := Value.Text;
	GetParameter := 0.0;	// TODO : 未実装
end;

{**
 * バックスペース
 * @return True 	成功
 * @return False	失敗
 *}
function TFormCalc.BackSpace: Boolean;
var
	nParamLen : Integer;
	strParam : string;
begin
	strParam := Value.Text;
	nParamLen := Length(strParam);

	if (nParamLen = 1) then begin
		strParam := '0'
	end else begin
		strParam := Copy(strParam, 1, (nParamLen - 1));
	end;

	SetParameter(strParam);

	Backspace := true;
end;

end.
