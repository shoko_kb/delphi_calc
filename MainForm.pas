/// メインフォーム
/// @author		小林　祥子
/// @version	2012/4/20 v0.1.0 初期バージョン
unit MainForm;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
	System.Classes, Vcl.Graphics,
	Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls, Vcl.ExtCtrls;

type
	/// 演算子
	TOperator = (
		/// @enum 加算
		opAdd,
		/// @enum 減算
		opSubstract,
		/// @enum 乗算
		opMultiply,
		/// @enum 除算
		opDivide);

	/// 電卓フォーム
	TFormCalc = class(TForm)
		/// ボタン[浮動小数点]
		ButtonFloatingPoint: TButton;
		/// ボタン[0]
		Button0: TButton;
		/// ボタン[1]
		Button1: TButton;
		/// ボタン[2]
		Button2: TButton;
		/// ボタン[3]
		Button3: TButton;
		/// ボタン[4]
		Button4: TButton;
		/// ボタン[5]
		Button5: TButton;
		/// ボタン[6]
		Button6: TButton;
		/// ボタン[7]
		Button7: TButton;
		/// ボタン[8]
		Button8: TButton;
		/// ボタン[9]
		Button9: TButton;
		/// ボタン[加算]
		ButtonAdd: TButton;
		/// ボタン[減算]
		ButtonSubtract: TButton;
		/// ボタン[乗算]
		ButtonMultiply: TButton;
		/// ボタン[除算]
		ButtonDivide: TButton;
		/// ボタン[C]
		ButtonClear: TButton;
		/// ボタン[←]
		ButtonBackSpace: TButton;
		/// ボタン[＝]
		ButtonEqual: TButton;
		/// エディットボックス[数値表示部分]
		EditValue: TEdit;
		/// メインメニュー
		MainMenu: TMainMenu;
		/// メニュー[桁区切り表示]
		MenuItemDigitGrouping: TMenuItem;
		procedure FormCreate(sender: TObject);
		procedure Button0Click(sender: TObject);
		procedure Button1Click(sender: TObject);
		procedure Button2Click(sender: TObject);
		procedure Button3Click(sender: TObject);
		procedure Button4Click(sender: TObject);
		procedure Button5Click(sender: TObject);
		procedure Button6Click(sender: TObject);
		procedure Button7Click(sender: TObject);
		procedure Button8Click(sender: TObject);
		procedure Button9Click(sender: TObject);
		procedure ButtonClearClick(sender: TObject);
		procedure ButtonFloatingPointClick(sender: TObject);
		procedure ButtonAddClick(sender: TObject);
		procedure ButtonSubtractClick(sender: TObject);
		procedure ButtonMultiplyClick(sender: TObject);
		procedure ButtonDivideClick(sender: TObject);
		procedure ButtonBackSpaceClick(sender: TObject);
		procedure ButtonEqualClick(sender: TObject);
		procedure FormKeyPress(sender: TObject; var key: Char);
		procedure FormKeyDown(sender: TObject; var key: Word; shift: TShiftState);
		procedure MenuItemDigitGroupingClick(sender: TObject);
	private
	{ Private 宣言 }
		const
		/// 入力最大桁数
		VALUE_MAX_LENGTH = 12;
		/// 最大入力値
		MAX_VALUE = 999999999999;
		/// 最少入力値
		MIN_VALUE = -99999999999;

	var
		/// 四則演算
		m_TOperator: TOperator;
		/// 現在の計算値
		m_currentValue: Double;
		/// 引数
		m_parameter: Double;
		/// エラー判定(true:エラー時, false:正常時)
		m_isError: Boolean;
		/// 演算命令のセット状態(true:演算命令がセットされている, false:演算命令がセットされていない)
		m_isCalcComplete: Boolean;
		procedure AppendNumber(newNumber: string);
		procedure ExecuteAllClear;
		procedure ExecuteBackSpace;
		procedure ExecuteAdd;
		procedure ExecuteSubstract;
		procedure ExecuteMultiply;
		procedure ExecuteDivide;
		procedure ExecuteEqual;
		procedure Update(newValue: string);
		procedure Calc;
	end;

var
	/// 電卓メインフォーム
	FormCalc: TFormCalc;

implementation

{$R *.dfm}

/// ボタン[9]押下時､「9」を入力する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.Button9Click(sender: TObject);
begin

	AppendNumber('9');
	DefocusControl(Button9, false);

end;

/// ボタン[8]押下時､「8」を入力する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.Button8Click(sender: TObject);
begin

	AppendNumber('8');
	DefocusControl(Button8, false);

end;

/// ボタン[7]押下時､「7」を入力する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.Button7Click(sender: TObject);
begin

	AppendNumber('7');
	DefocusControl(Button7, false);

end;

/// ボタン[6]押下時､「6」を入力する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.Button6Click(sender: TObject);
begin

	AppendNumber('6');
	DefocusControl(Button6, false);

end;

/// ボタン[5]押下時､「5」を入力する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.Button5Click(sender: TObject);
begin

	AppendNumber('5');
	DefocusControl(Button5, false);

end;

/// ボタン[4]押下時､「4」を入力する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.Button4Click(sender: TObject);
begin

	AppendNumber('4');
	DefocusControl(Button4, false);

end;

/// ボタン[3]押下時､「3」を入力する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.Button3Click(sender: TObject);
begin

	AppendNumber('3');
	DefocusControl(Button3, false);

end;

/// ボタン[2]押下時､「2」を入力する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.Button2Click(sender: TObject);
begin

	AppendNumber('2');
	DefocusControl(Button2, false);

end;

/// ボタン[1]押下時､「1」を入力する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.Button1Click(sender: TObject);
begin

	AppendNumber('1');
	DefocusControl(Button1, false);

end;

/// ボタン[0]押下時､「0」を入力する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.Button0Click(sender: TObject);
begin

	AppendNumber('0');
	DefocusControl(Button0, false);

end;

/// ボタン[加算]押下時､加算命令をセットする。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.ButtonAddClick(sender: TObject);
begin

	ExecuteAdd;

end;

/// ボタン[減算]押下時､減算命令をセットする。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.ButtonSubtractClick(sender: TObject);
begin

	ExecuteSubstract;

end;

/// ボタン[乗算]押下時､乗算命令をセットする。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.ButtonMultiplyClick(sender: TObject);
begin

	ExecuteMultiply;

end;

/// ボタン[除算]押下時､除算命令をセットする。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.ButtonDivideClick(sender: TObject);
begin

	ExecuteDivide;

end;

/// ボタン[クリア]押下時､計算を初期化する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.ButtonClearClick(sender: TObject);
begin

	ExecuteAllClear;

end;

/// ボタン[バックスペース]押下時､変数の末尾数字を削除する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.ButtonBackSpaceClick(sender: TObject);
begin

	ExecuteBackSpace;

end;

/// ボタン[イコール]押下時、計算を実行する。<br/>
/// 2回連続の実行の際には、現在値にたいして前回の引数で計算を実行する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.ButtonEqualClick(sender: TObject);
begin

	ExecuteEqual;

end;

/// ボタン[浮動小数点]押下時、浮動小数点を入力する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.ButtonFloatingPointClick(sender: TObject);
begin

	AppendNumber('.');

end;

/// フォーム作成時､画面を初期化する。
/// @param	sender	送信元オブジェクト
procedure TFormCalc.FormCreate(sender: TObject);
begin

	ExecuteAllClear;

end;

/// キーダウン処理<br/>
/// [	.]:浮動小数点処理<br/>
/// [Enter]:計算実行処理
/// [Backspace]:末尾数字削除処理<br/>
/// [Delete]:オールクリア処理
/// @param	sender	送信元オブジェクト
procedure TFormCalc.FormKeyDown(sender: TObject; var key: Word;
	shift: TShiftState);
begin

	case key of
		VK_RETURN:
			ExecuteEqual;
		VK_BACK:
			ExecuteBackSpace;
		VK_DELETE:
			ExecuteAllClear;
	end;

end;

/// キープレス処理<br/>
/// [1-9]:数字入力処理<br/>
/// [+,-,, /]:演算子設定・計算実行処理<br/>
/// [=]:計算実行処理
/// @param	sender	送信元オブジェクト
procedure TFormCalc.FormKeyPress(sender: TObject; var key: Char);
begin

	case key of
		'0':
			AppendNumber('0');
		'1':
			AppendNumber('1');
		'2':
			AppendNumber('2');
		'3':
			AppendNumber('3');
		'4':
			AppendNumber('4');
		'5':
			AppendNumber('5');
		'6':
			AppendNumber('6');
		'7':
			AppendNumber('7');
		'8':
			AppendNumber('8');
		'9':
			AppendNumber('9');
		'.':
			AppendNumber('.');
		'+':
			ExecuteAdd;
		'-':
			ExecuteSubstract;
		'*':
			ExecuteMultiply;
		'/':
			ExecuteDivide;
		'=':
			ExecuteEqual;
	end;

end;

/// 桁区切りを設定する
/// @param	sender	送信元オブジェクト
procedure TFormCalc.MenuItemDigitGroupingClick(sender: TObject);
begin

	MenuItemDigitGrouping.Checked := not MenuItemDigitGrouping.Checked;

	if (m_isCalcComplete <> false) then
	begin

		Update(FloatToStr(m_currentValue));

	end
	else
	begin

		Update(EditValue.Text);

	end;

end;

/// 数字を設定する。
/// @param	newValue	表示値
procedure TFormCalc.Update(newValue: string);
var
	value: Double; // 表示値(数値)
	valueText: string; // 表示文字列
	valueLength: Integer; // 文字列長
	decimalNumber: Integer; // 小数点以下の桁数
	floatingPointPos: Integer; // 小数点の位置
begin

	if (m_isError <> false) then
	begin

		// エラーの時は'E’を表示する
		valueText := 'E';

	end
	else
	begin

		// 最大文字列長を超過しない限り、文字列を入力する
		valueLength := Length(newValue);

		if (valueLength < VALUE_MAX_LENGTH) then
		begin
			valueText := newValue;
		end
		else
		begin
			valueText := Copy(newValue, 1, VALUE_MAX_LENGTH);
		end;

		// 小数点以下の桁数を取得する
		floatingPointPos := Pos('.', valueText);
		if (floatingPointPos = 0) then
		begin
			decimalNumber := 0;
		end
		else if (valueLength > VALUE_MAX_LENGTH) then
		begin
			decimalNumber := VALUE_MAX_LENGTH - Pos('.', valueText);
		end
		else
		begin
			decimalNumber := valueLength - Pos('.', valueText);
		end;

		// 浮動小数点の数値に変換する
		while (Pos(',', valueText) > 0) do
		begin
			Delete(valueText, Pos(',', valueText), 1);
		end;
		value := StrToFloat(valueText);

		// 桁区切りが設定されているときには、桁区切りで表示する
		if (MenuItemDigitGrouping.Checked <> false) then
		begin
			valueText := FloatToStrF(value, ffNumber, 15, decimalNumber);
		end
		else
		begin
			valueText := FloatToStrF(value, ffFixed, 15, decimalNumber);
		end;

		// 小数点末尾に存在するときは小数点を追加する
		if (valueLength = floatingPointPos) then
		begin
			valueText := valueText + '.';
		end;

	end;

	EditValue.Text := valueText;

end;

/// 数字を追加する。
/// @param	newNumber	追加する数字
procedure TFormCalc.AppendNumber(newNumber: string);
var
	parameter: string; // 結果表示ラベルの文字列
	checkFloatingPoint: Integer; // 浮動小数点を含むかを判断するフラグ
	// (含む場合:0以外、含まない場合:0)
	valueLength: Integer; // 表示値の文字列長
begin

	valueLength := Length(EditValue.Text);

	if (m_isError = false) then
	begin

		// 計算が終了している場合、変数は0を設定する。
		// それ以外の場合は、表示値を設定する
		if (m_isCalcComplete <> false) then
		begin
			parameter := '0';
		end
		else
		begin
			parameter := EditValue.Text;
		end;

		if (newNumber = '.') then
		begin

			// 追加する数字が浮動小数点の場合、
			// 浮動小数点を2つ以上追加しないようにする
			checkFloatingPoint := Pos('.', parameter);
			if (checkFloatingPoint = 0) then
			begin
				parameter := parameter + newNumber
			end;

		end
		else
		begin

			// 数字を入力する場合には
			// 直前の入力値が0の場合入力数字を上書き、
			// そのほかの場合は数字を末尾に追加する
			if ((m_isCalcComplete <> false) or (EditValue.Text = '0')) then
			begin

				parameter := newNumber

			end
			else if (valueLength < VALUE_MAX_LENGTH) then
			begin

				parameter := parameter + newNumber;

			end
			else
			begin
				//
			end;

		end;

		// 表示値を更新する
		Update(parameter);

		m_isCalcComplete := false;

	end;

end;

/// 変数の末尾数字を削除する。
procedure TFormCalc.ExecuteBackSpace;
var
	parameterLength: Integer;
	parameter: string;
begin

	if m_isCalcComplete = false then
	begin

		parameter := EditValue.Text;
		parameterLength := Length(parameter);

		if (parameterLength = 1) then
		begin
			parameter := '0';
		end
		else
		begin
			parameter := Copy(parameter, 1, (parameterLength - 1));
		end;

		Update(parameter);

	end;

end;

/// 計算を初期化する。
procedure TFormCalc.ExecuteAllClear;
begin

	m_currentValue := 0;
	m_parameter := 0;
	m_TOperator := opAdd;
	m_isError := false;
	m_isCalcComplete := true;
	Update(FloatToStr(m_currentValue));

end;

/// 加算をセットする。
procedure TFormCalc.ExecuteAdd;
var
	valueText: string;
begin

	// 数値入力が新たに入力されているときのみ、
	// 変数を更新し、演算を実行する
	valueText := EditValue.Text;
	if ((m_isError = false) and (m_isCalcComplete = false)) then
	begin

		// 浮動小数点の数値に変換する
		while (Pos(',', valueText) > 0) do
		begin
			Delete(valueText, Pos(',', valueText), 1);
		end;
		m_parameter := StrToFloat(valueText);

		Calc;

	end;

	m_TOperator := opAdd;

end;

/// 減算をセットする。
procedure TFormCalc.ExecuteSubstract;
var
	valueText: string;
begin

	// 数値入力が新たに入力されているときのみ、
	// 変数を更新し、演算を実行する
	valueText := EditValue.Text;
	if ((m_isError = false) and (m_isCalcComplete = false)) then
	begin

		// 浮動小数点の数値に変換する
		while (Pos(',', valueText) > 0) do
		begin
			Delete(valueText, Pos(',', valueText), 1);
		end;
		m_parameter := StrToFloat(valueText);

		Calc;

	end;

	m_TOperator := opSubstract;

end;

/// 乗算をセットする。
procedure TFormCalc.ExecuteMultiply;
var
	valueText: string;
begin

	// 数値入力が新たに入力されているときのみ、
	// 変数を更新し、演算を実行する
	valueText := EditValue.Text;
	if ((m_isError = false) and (m_isCalcComplete = false)) then
	begin

		// 浮動小数点の数値に変換する
		while (Pos(',', valueText) > 0) do
		begin
			Delete(valueText, Pos(',', valueText), 1);
		end;
		m_parameter := StrToFloat(valueText);

		Calc;

	end;

	m_TOperator := opMultiply;

end;

/// 除算をセットする。
procedure TFormCalc.ExecuteDivide;
var
	valueText: string;
begin

	// 数値入力が新たに入力されているときのみ、
	// 変数を更新し、演算を実行する
	valueText := EditValue.Text;
	if ((m_isError = false) and (m_isCalcComplete = false)) then
	begin

		// 浮動小数点の数値に変換する
		while (Pos(',', valueText) > 0) do
		begin
			Delete(valueText, Pos(',', valueText), 1);
		end;
		m_parameter := StrToFloat(valueText);

		Calc;

	end;

	m_TOperator := opDivide;

end;

/// 計算を実行する。
procedure TFormCalc.ExecuteEqual;
var
	valueText: string;
begin

	// 数値入力が新たに入力されているときのみ、
	// 変数を更新し、演算を実行する
	valueText := EditValue.Text;
	if ((m_isError = false) and (m_isCalcComplete = false)) then
	begin

		// 浮動小数点の数値に変換する
		while (Pos(',', valueText) > 0) do
		begin
			Delete(valueText, Pos(',', valueText), 1);
		end;
		m_parameter := StrToFloat(valueText);

	end;

	Calc;

end;

/// 計算処理を実行し、表示を更新する。
procedure TFormCalc.Calc;
begin

	// エラー発生中は演算を続行しない
	if (m_isError = false) then
	begin

		// 四則演算命令に従い、演算を実行する
		if (m_TOperator = opAdd) then
		begin
			m_currentValue := m_currentValue + m_parameter;
		end
		else if (m_TOperator = opSubstract) then
		begin
			m_currentValue := m_currentValue - m_parameter;
		end
		else if (m_TOperator = opMultiply) then
		begin
			m_currentValue := m_currentValue * m_parameter;
		end
		else if (m_TOperator = opDivide) then
		begin

			// 0で除算する場合はエラーを発生する
			if (m_parameter = 0) then
			begin
				m_isError := true;
			end
			else
			begin
				m_currentValue := m_currentValue / m_parameter;
			end;

		end;

		// 現在値が最大値・最小値を超過した場合はエラーを発生する
		if ((m_currentValue < MIN_VALUE) or (m_currentValue > MAX_VALUE)) then
		begin
			m_isError := true;
		end;

		// 計算処理を終了する
		m_isCalcComplete := true;

		// 表示値を更新する
		Update(FloatToStr(m_currentValue));

	end;

end;

end.
