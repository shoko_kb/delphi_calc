object FormEncdDecdBase64: TFormEncdDecdBase64
  Left = 0
  Top = 0
  Caption = 'FormEncdDecdBase64'
  ClientHeight = 243
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ButtonEncode: TButton
    Left = 8
    Top = 215
    Width = 75
    Height = 25
    Caption = #12456#12531#12467#12540#12489
    TabOrder = 0
    OnClick = ButtonEncodeClick
  end
  object ButtonDecode: TButton
    Left = 89
    Top = 215
    Width = 75
    Height = 25
    Caption = #12487#12467#12540#12489
    TabOrder = 1
    OnClick = ButtonDecodeClick
  end
  object Memo: TMemo
    Left = 8
    Top = 8
    Width = 456
    Height = 201
    Lines.Strings = (
      'Memo')
    ScrollBars = ssBoth
    TabOrder = 2
  end
end
